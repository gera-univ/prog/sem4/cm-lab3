class LFSR:
    def __init__(self, taps, seed):
        assert max(taps) <= len(seed) and min(taps) > 0
        self.length = len(seed)
        self.taps = taps
        self.seed = int(seed, 2)
        self.register = self.seed

    def make_shift(self):
        new_bit = 0
        for tap in self.taps:
            new_bit ^= self.register >> tap - 1
        new_bit &= 1
        self.register = (self.register << 1) | new_bit
        self.register &= (1 << self.length) - 1
        return new_bit

    def get_period(self):
        result = 1
        self.make_shift()
        while self.register != self.seed:
            self.make_shift()
            result += 1
        return result


if __name__ == "__main__":
    reg = LFSR({3, 5}, "11010")
    while True:
        print(f"{reg.register:>05b}")
        reg.make_shift()
        if reg.register == reg.seed:
            break
