from lfsr import LFSR


class Geffe:
    def __init__(self, p1, s1, p2, s2, p3, s3):
        self.reg1 = LFSR(p1, s1)
        self.reg2 = LFSR(p2, s2)
        self.reg3 = LFSR(p3, s3)

    def next_bit(self):
        s1 = self.reg1.make_shift()
        s2 = self.reg2.make_shift()
        s3 = self.reg3.make_shift()
        return (s1 & s2) ^ ((s1 ^ 1) * s3)

    def get_reg_periods(self):
        return [g.get_period() for g in (self.reg1, self.reg2, self.reg3)]

    def get_max_period(self):
        return (2**(self.reg1.length) - 1) * (2**(self.reg2.length) - 1) * (2**(self.reg3.length) - 1)


if __name__ == "__main__":
    count = 10000
    gen = Geffe({5, 2}, "01101", {7, 6, 5, 4, 2, 1}, "1000010", {8, 6, 5, 4}, "00110010")
    sequence = [gen.next_bit() for _ in range(count)]
    zero_count = sequence.count(0)
    print(f"periods:{gen.get_reg_periods()}")
    print(f"max_period:{gen.get_max_period()}")
    print(f"cnt 0:\t{zero_count}")
    print(f"cnt 1:\t{len(sequence) - zero_count}")
    tau = lambda x: (1, -1)[x % 2]  # same as (-1) ** x
    for i in range(1, 6):
        r = 0
        for j in range(0, count - i, i):
            r += tau(sequence[j] ^ sequence[j + i])
        print(f"r_{i}:\t{r:+d}")
    file = open("sequence.txt", "w")
    file.write("".join(map(str, sequence)))
    file.close()
